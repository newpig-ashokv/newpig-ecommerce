resource "aws_api_gateway_resource" "Resource_sitemap" {
  path_part = "${var.resource_sitemap}"
  parent_id = "${aws_api_gateway_resource.Resource_root.id}"
  rest_api_id = "${aws_api_gateway_rest_api.MyAPI.id}"
}

resource "aws_api_gateway_resource" "Resource_lookup" {
  path_part = "${var.resource_lookup}"
  parent_id = "${aws_api_gateway_resource.Resource_sitemap.id}"
  rest_api_id = "${aws_api_gateway_rest_api.MyAPI.id}"
}

resource "aws_api_gateway_resource" "Resource_product" {
  path_part = "${var.resource_product}"
  parent_id = "${aws_api_gateway_resource.Resource_lookup.id}"
  rest_api_id = "${aws_api_gateway_rest_api.MyAPI.id}"
}

# API Gateway to handle method
resource "aws_api_gateway_method" "Product_method" {
  authorization = "NONE"
  http_method = "${var.http_method}"
  resource_id = "${aws_api_gateway_resource.Resource_product.id}"
  rest_api_id = "${aws_api_gateway_rest_api.MyAPI.id}"
  api_key_required = true
}

# An integration between Lambda and API Gateway
resource "aws_api_gateway_integration" "Product_Intigration" {
  http_method = "${aws_api_gateway_method.Product_method.http_method}"
  resource_id = "${aws_api_gateway_resource.Resource_product.id}"
  rest_api_id = "${aws_api_gateway_rest_api.MyAPI.id}"
  type = "AWS"
  integration_http_method = "POST"
  uri = "arn:aws:apigateway:${var.region}:lambda:path/2015-03-31/functions/arn:aws:lambda:${var.region}:${var.account_id}:function:${var.lambda_product}/invocations"
  request_templates {
    "application/json" = "$input.params('product')"
  }
}

# lambda => GET response
resource "aws_api_gateway_method_response" "Product_response_method" {
  rest_api_id = "${aws_api_gateway_rest_api.MyAPI.id}"
  resource_id = "${aws_api_gateway_resource.Resource_product.id}"
  http_method = "${aws_api_gateway_method.Product_method.http_method}"
  status_code = "200"
  response_models = {
    "application/json" = "Empty"
  }
}

# Response for: GET /hello
resource "aws_api_gateway_integration_response" "Product_response_integration" {
  rest_api_id = "${aws_api_gateway_rest_api.MyAPI.id}"
  resource_id = "${aws_api_gateway_resource.Resource_product.id}"
  http_method = "${aws_api_gateway_method.Product_method.http_method}"
  status_code = "${aws_api_gateway_method_response.Product_response_method.status_code}"
  response_templates {
    "application/json" = ""
  }
}

resource "aws_lambda_permission" "product_api_gateway_lambda_permission" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "${var.lambda_product}"
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${var.region}:${var.account_id}:${aws_api_gateway_rest_api.MyAPI.id}/*/*"
}