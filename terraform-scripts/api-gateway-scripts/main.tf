provider "aws" {
  region     = "${var.region}"
}


# API Gateway Root
resource "aws_api_gateway_rest_api" "MyAPI" {
  name  = "${var.rest_api_id}"
  description = "Created for testing"
}


# API Gateway resource
resource "aws_api_gateway_resource" "Resource_root" {
  path_part = "${var.resource_root}"
  parent_id = "${aws_api_gateway_rest_api.MyAPI.root_resource_id}"
  rest_api_id = "${aws_api_gateway_rest_api.MyAPI.id}"
}

resource "aws_api_gateway_resource" "Resource_identity" {
  path_part = "${var.resource_identity}"
  parent_id = "${aws_api_gateway_resource.Resource_root.id}"
  rest_api_id = "${aws_api_gateway_rest_api.MyAPI.id}"
}

resource "aws_api_gateway_resource" "Resource" {
  path_part = "${var.resource_value}"
  parent_id = "${aws_api_gateway_resource.Resource_identity.id}"
  rest_api_id = "${aws_api_gateway_rest_api.MyAPI.id}"
}

# API Gateway to handle method
resource "aws_api_gateway_method" "MyMethod" {
  authorization = "NONE"
  http_method = "${var.http_method}"
  resource_id = "${aws_api_gateway_resource.Resource.id}"
  rest_api_id = "${aws_api_gateway_rest_api.MyAPI.id}"
  api_key_required = true
}

# An integration between Lambda and API Gateway
resource "aws_api_gateway_integration" "MyapiIntigration" {
  http_method = "${aws_api_gateway_method.MyMethod.http_method}"
  resource_id = "${aws_api_gateway_resource.Resource.id}"
  rest_api_id = "${aws_api_gateway_rest_api.MyAPI.id}"
  type = "AWS"
  integration_http_method = "POST"
  uri = "arn:aws:apigateway:${var.region}:lambda:path/2015-03-31/functions/arn:aws:lambda:${var.region}:${var.account_id}:function:${var.lambda_name}/invocations"
}

# lambda => GET response
resource "aws_api_gateway_method_response" "response_method" {
  rest_api_id = "${aws_api_gateway_rest_api.MyAPI.id}"
  resource_id = "${aws_api_gateway_resource.Resource.id}"
  http_method = "${aws_api_gateway_method.MyMethod.http_method}"
  status_code = "200"
  response_models = {
    "application/json" = "Empty"
  }
}

# Response for: GET /hello
resource "aws_api_gateway_integration_response" "response_method_integration" {
  rest_api_id = "${aws_api_gateway_rest_api.MyAPI.id}"
  resource_id = "${aws_api_gateway_resource.Resource.id}"
  http_method = "${aws_api_gateway_method.MyMethod.http_method}"
  status_code = "${aws_api_gateway_method_response.response_method.status_code}"
  response_templates {
    "application/json" = ""
  }
}


resource "aws_lambda_permission" "api_gateway_lambda_permission" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "${var.lambda_name}"
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${var.region}:${var.account_id}:${aws_api_gateway_rest_api.MyAPI.id}/*/*"
}

# API Gateway deployment stage
resource "aws_api_gateway_deployment" "example_deployment_dev" {
  depends_on = [
    "aws_api_gateway_method.MyMethod",
    "aws_api_gateway_integration.MyapiIntigration"
  ]
  rest_api_id = "${aws_api_gateway_rest_api.MyAPI.id}"
  stage_name = "dev"
}


output "dev_url" {
  value = "https://${aws_api_gateway_deployment.example_deployment_dev.rest_api_id}.execute-api.${var.region}.amazonaws.com/${aws_api_gateway_deployment.example_deployment_dev.stage_name}"
}