variable "rest_api_id" {
  type = "string"
  description = "name of the api gateway id"
}

variable "resource_root" {
  type = "string"
  description = "name of the resource id"
}
variable "resource_identity" {
  type = "string"
  description = "name of the resource id"
}
variable "resource_value" {
  type = "string"
  description = "name of the resource id"
}
variable "resource_recycling" {
  type = "string"
  description = "name of the resource id"
}
variable "resource_lighting" {
  type = "string"
  description = "name of the resource id"
}
variable "resource_newpig_id" {
  type = "string"
  description = "name of the resource id"
}

variable "resource_sitemap" {
  type = "string"
  description = "name of the resource id"
}

variable "resource_lookup" {
  type = "string"
  description = "name of the resource id"
}

variable "resource_product" {
  type = "string"
  description = "name of the resource id"
}

variable "resource_zipcode" {
  type = "string"
  description = "name of the resource id"
}

variable "resource_US" {
  type = "string"
  description = "name of the resource id"
}

variable "resource_zipcode_id" {
  type = "string"
  description = "name of the resource id"
}


variable "http_method" {
  type = "string"
  description = "http method id"
  default = "GET"
}
variable "lambda_name" {
  type = "string"
  description = "Name of Lambda function (required)"
}

variable "lambda_newpig_id" {
  type = "string"
  description = "Name of Lambda function (required)"
}
variable "lambda_product" {
  type = "string"
  description = "Name of Lambda function (required)"
}
variable "lambda_zipcode" {
  type = "string"
  description = "Name of Lambda function (required)"
}
variable "region" {
  type = "string"
  description = "region required for api and lamda"
  default = "us-east-1"
}

variable "account_id" {
  type = "string"
  description = "account ID for AWS account"
  default = "710460016152"
}
