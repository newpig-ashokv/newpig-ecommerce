provider "aws" {
  region = "${var.aws_region}"
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
}

resource "aws_eip" "nphybristesting" {
  vpc = true
  network_interface = "${aws_network_interface.eth1.id}"
  tags {Name = "${var.instance_name}-eip"}
}


resource "aws_network_interface" "eth0" {
  subnet_id = "${var.PPrivate-Subnet}"
  #private_ips = ["10.100.100.181"]
  security_groups = ["${var.Global_security-group}"]

  tags {
    Name = "primary_network_interface"
  }
}

resource "aws_network_interface" "eth1" {
  subnet_id = "${var.PPublic-Subnet}"
  #private_ips = ["10.100.200.198"]
  security_groups = ["${var.public-nat-ssh-newpig}","${var.private-lan-icmp}","${var.private-lan-lucidworks}","${var.private-lan-mysql}","${var.private-lan-fusion}","${var.public-elb-3dsi}","${var.private-lan-ssh}","${var.private-lan-hybris}","${var.public-elb-newpig}","${var.private-lan-http-standard}"]

  tags {
    Name = "secondary_network_interface"
  }
}


resource "aws_instance" "foo" {
  ami = "${var.ami}"
  instance_type = "${var.instance_type}"
  key_name = "${var.keypair_name}"
  ebs_block_device {
    device_name = "/dev/xvda"
    volume_size = 100
    volume_type = "gp2"
    delete_on_termination = true
    #encrypted = true
  }

  network_interface {
    network_interface_id = "${aws_network_interface.eth0.id}"
    device_index = 0
  }
  network_interface {
    network_interface_id = "${aws_network_interface.eth1.id}"
    device_index = 1
  }
  tags {Name = "${var.instance_name}"}
}