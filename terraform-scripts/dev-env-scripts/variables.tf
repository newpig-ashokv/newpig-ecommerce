variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "aws_region" {
  description = "EC2 Region for the VPC"
  default = "us-east-1"
}

variable "vpc_id" {
  type = "string"
  description = "vpc"
  default = "vpc-0974a46c"
}

variable "ami" {
  type = "string"
  description = "ami to create an dev instance"
  default = "ami-8bd30df4"
}

variable "keypair_name" {
  type = "string"
  description = "keypair to create the instances in Development environment"
  default = "aws-dev"
}

variable "instance_name" {
  default = "np-hybris-default"
}

variable "instance_type" {
  type = "string"
  description = "type of an instance to be created"
  default = "t2.micro"
}

###########################################################
###########################################################
####                 Subnets
###########################################################
###########################################################



variable "PPrivate-Subnet" {
  type = "string"
  description = "Primary Private Instance Subnet"
  default = "subnet-280d3e00"
}

variable "PPublic-Subnet" {
  type = "string"
  description = "Primary Private Instance Subnet"
  default = "subnet-3d0d3e15"
}


###########################################################
###########################################################
####                 security groups
###########################################################
###########################################################


variable "Global_security-group" {
  type = "string"
  description = "default security group which allows all traffic"
  default = "sg-3f8ef45a"
}

variable "public-nat-ssh-newpig" {
  type = "string"
  default = "sg-24e2a041"
}
variable "private-lan-icmp" {
  type = "string"
  default = "sg-71621a14"
}

variable "private-lan-lucidworks" {
  type = "string"
  default = "sg-9fb2e0fa"
}

variable "private-lan-mysql" {
  type = "string"
  default = "sg-2fc8b14a"
}

variable "private-lan-fusion" {
  type = "string"
  default = "sg-9dad67f9"
}

variable "public-elb-3dsi" {
  type = "string"
  default = "sg-aa94efce"
}

variable "private-lan-ssh" {
  type = "string"
  default = "sg-0aaed76f"
}

variable "private-lan-hybris" {
  type = "string"
  default = "sg-b0a0dbd5"
}

variable "public-elb-newpig" {
  type = "string"
  default = "sg-22f08a47"
}

variable "private-lan-http-standard" {
  type = "string"
  default = "sg-c5e1a3a0"
}
